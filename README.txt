/**
 * @file
 * README file for Workbench Access Content type.
 */

Workbench Access Content type
Extensible editorial access for the Workbench suite for content types.

This module will add a new tab "Content Types" at the configuration page of Workbench Access. to let admins let sections have access to create content.
