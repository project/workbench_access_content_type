<?php

/**
 * @file
 * Workbench Access content type admin file.
 */

/**
 * Settings form for Workbench Access Content type configuration.
 */
function workbench_access_content_type_settings_form($form, &$form_state) {
  $form = array();
  $form['help'] = array(
    '#markup' => '<h2>' . t('Workbench Access content types settings') . '</h2>',
  );

  // Get list of content types.
  $content_types = node_type_get_types();

  // Get list of workbench access taxonomis.
  $workbench_access_taxonomis = array_filter(variable_get('workbench_access_taxonomy', array()));

  // Have a list of operations.
  $operations = array(
    // 'view' => 'View',
    'create' => 'Create',
    'update_own' => 'Update own',
    'update' => 'Update',
    // 'delete' => 'Delete',
  );

  if (!empty($content_types)) {
    foreach ($content_types as $content_type_name => $content_type_data) {
      $form['workbench_access_content_type_' . $content_type_name] = array(
        '#type' => 'fieldset',
        '#title' => check_plain($content_type_data->name),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
      foreach ($workbench_access_taxonomis as $workbench_access_taxonomy) {
        $taxonomy_vocabulary =  taxonomy_vocabulary_machine_name_load($workbench_access_taxonomy);

        $sections = taxonomy_get_tree($taxonomy_vocabulary->vid);

        $form['workbench_access_content_type_' . $content_type_name][$taxonomy_vocabulary->machine_name] = array(
          '#type' => 'fieldset',
          '#title' => check_plain($taxonomy_vocabulary->name),
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
        );

        foreach ($operations as $operation_key => $operation_name) {

          $form['workbench_access_content_type_' . $content_type_name][$taxonomy_vocabulary->machine_name][$operation_key] = array(
                                                                            '#type' => 'fieldset',
                                                                            '#title' => check_plain($operation_name),
                                                                            '#collapsible' => TRUE,
                                                                            '#collapsed' => FALSE,
                                                                          );

          // Add the All of taxonomy_vocabulary sections.
          $form['workbench_access_content_type_' . $content_type_name][$taxonomy_vocabulary->machine_name][$operation_key]['workbench_access_content_type_' . $operation_key . '_' . $content_type_name . '_all'] = array(
            '#title' =>  'All of ' . $taxonomy_vocabulary->name,
             '#type' => 'checkbox',
             '#default_value' => variable_get('workbench_access_content_type_' . $operation_key . '_' . $content_type_name . '_all', 0),
            );

          foreach ($sections as $section) {
            $dashed = _workbench_access_content_type_dashed($section->depth+1);
            $form['workbench_access_content_type_' . $content_type_name][$taxonomy_vocabulary->machine_name][$operation_key]['workbench_access_content_type_' . $operation_key . '_' . $content_type_name . '_' . $section->tid] = array(
              '#title' =>  $dashed . $section->name,
              '#type' => 'checkbox',
              '#default_value' => variable_get('workbench_access_content_type_' . $operation_key . '_' . $content_type_name . '_' . $section->tid, 0),
            );
          }

          // Add the Restrict all taxonomy_vocabulary sections.
          $form['workbench_access_content_type_' . $content_type_name][$taxonomy_vocabulary->machine_name][$operation_key]['workbench_access_content_type_' . $operation_key . '_' . $content_type_name . '_restrict_all'] = array(
            '#title' =>  ' #### Restrict all ' . $taxonomy_vocabulary->name . ' #### ',
            '#type' => 'checkbox',
            '#default_value' => variable_get('workbench_access_content_type_' . $operation_key . '_' . $content_type_name . '_restrict_all', 0),
          );
        }
      }
    }
  }

  $form = system_settings_form($form);
  return $form;
}

/**
 *  Repeat dashed with the number of dashed.
 */
function _workbench_access_content_type_dashed($number_of_dashed) {
  if ($number_of_dashed > 0) {
    return ' ' . str_repeat('-', $number_of_dashed) . ' ';
  }
  else {
    return '';
  }
}
