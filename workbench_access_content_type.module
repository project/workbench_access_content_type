<?php

/**
 * @file
 * Workbench Access Content Type module file.
 */

/**
 * Implements hook_menu().
 */
function workbench_access_content_type_menu() {
  $items['admin/config/workbench/access/content-types'] = array(
    'title' => 'Content types',
    'description' => 'The list of content types for editors ',
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'workbench_access_content_type_settings_form'
    ),
    'access arguments' => array(
      'administer workbench access'
    ),
    'type' => MENU_LOCAL_TASK,
    'weight' => -11,
    'file' => 'workbench_access_content_type.admin.inc'
  );

  return $items;
}

/**
 * Implements hook_node_access().
 *
 * Enforces our access rules when users try to create a node.
 */
function workbench_access_content_type_node_access($node, $op, $account) {
  if (!isset($account->workbench_access)) {
    workbench_access_user_load_data($account);
  }

  // If not configured, do nothing.
  $tree = workbench_access_get_active_tree();
  if (empty($tree['active'])) {
    return NODE_ACCESS_IGNORE;
  }

  // View step. We ignore for published nodes.
  if ($op == 'view' && $node->status) {
    return NODE_ACCESS_IGNORE;
  }

  // If disabled for this content type, do nothing.
  if (!is_object($node)) {
    $type = $node;
  }
  else {
    $type = $node->type;
  }

  // Create step. User must be assigned to a section to create content.
  // Note that we do not currently enforce complex rules here.
  if ($op == 'create' && !empty($account->workbench_access)) {
    if (variable_get('workbench_access_content_type_create_' . $type . '_all', 0)) {
      return NODE_ACCESS_IGNORE;
    }
    elseif (variable_get('workbench_access_content_type_create_' . $type . '_restrict_all', 0)) {
      return NODE_ACCESS_DENY;
    }
    else {
      foreach ($account->workbench_access as $section_id => $section) {
        if (variable_get('workbench_access_content_type_create_' . $type . '_' . $section_id, 0)) {
          return NODE_ACCESS_IGNORE;
        }
      }

      return NODE_ACCESS_DENY;
    }
  }

  // Update step. User must be assigned to a section to update content.
  // Note that we do not currently enforce complex rules here.
  if ($op == 'update' && !empty($account->workbench_access)) {
    // Check on update own content
    if ($account->uid == $node->uid) {
      if (variable_get('workbench_access_content_type_update_own_' . $type . '_all', 0)) {
        return NODE_ACCESS_IGNORE;
      }
      elseif (variable_get('workbench_access_content_type_update_own_' . $type . '_restrict_all', 0)) {
        return NODE_ACCESS_DENY;
      }
      else {
        foreach ($account->workbench_access as $section_id => $section) {
          if (variable_get('workbench_access_content_type_update_own_' . $type . '_' . $section_id, 0)) {
            return NODE_ACCESS_IGNORE;
          }
        }
      }

      return NODE_ACCESS_DENY;
    }
    // Check on update other's content (not own)
    else {
      if (variable_get('workbench_access_content_type_update_' . $type . '_all', 0)) {
        return NODE_ACCESS_IGNORE;
      }
      elseif (variable_get('workbench_access_content_type_update_' . $type . '_restrict_all', 0)) {
        return NODE_ACCESS_DENY;
      }
      else {
        foreach ($account->workbench_access as $section_id => $section) {
          if (variable_get('workbench_access_content_type_update_' . $type . '_' . $section_id, 0)) {
            return NODE_ACCESS_IGNORE;
          }
        }
      }

      return NODE_ACCESS_DENY;
    }
  }

  // Let other modules deiced if the user can create or update.
  return NODE_ACCESS_IGNORE;
}
